class Integer

  def factorial
    (1..self).inject(1, :*)
  end

  def combination
    array = Array.new
    (0..self).each { |value| array.push((factorial) / ((self - value).factorial * value.factorial)) }
    array
  end

  def print_pascal_triangle
    (0..self).each { |value| yield value.combination }
  end

end
