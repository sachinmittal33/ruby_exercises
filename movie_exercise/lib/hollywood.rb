require_relative 'movie.rb'

class Hollywood < Movie

  @@hollywood_list = Array.new

  def initialize(name, release_date, genre)
    begin
      super(name, release_date, genre)
    rescue NoMethodError
    end
    @@hollywood_list.push(self)
  end

  def self.all
    @@hollywood_list.each { |movie| puts movie.name }
  end

end
