require_relative 'movie.rb'

class Bollywood < Movie

  @@bollywood_list = Array.new

  def initialize(name, release_date, genre)
    begin
      super(name, release_date, genre)
    rescue NoMethodError
    end
    @@bollywood_list.push(self)
  end

  def self.all
    @@bollywood_list.each { |movie| puts movie.name }
  end

end
