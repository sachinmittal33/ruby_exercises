require_relative 'movie.rb'

class Tollywood < Movie

  @@tollywood_list = Array.new

  def initialize(name, release_date, genre)
    begin
      super(name, release_date, genre)
    rescue NoMethodError
    end
    @@tollywood_list.push(self)
  end

  def self.all
    @@tollywood_list.each { |movie| puts movie.name }
  end

end
