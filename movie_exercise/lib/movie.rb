require 'date'

class Movie

  @@movie_list = Array.new

  attr_reader :name , :release_date , :genre

  def initialize(name, release_date, genre)
    @name = name
    @release_date = Date.parse(release_date)
    @genre = genre
    @@movie_list.push(self)
    raise NoMethodError
  end

  def > movie
    release_date > movie.release_date
  end

  def self.all
    @@movie_list.each { |movie| puts movie.name }
  end

end
