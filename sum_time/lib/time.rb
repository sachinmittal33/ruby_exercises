require 'time'

class Time

  MATCH_PATTERN = /^([01]?[0-9]|2[0-3]):[0-5]?[0-9]:[0-5]?[0-9]$/

  def self.validate!(string)
    raise RangeError if string !~ MATCH_PATTERN
  end

  def self.sum(string1, string2)
    validate!(string1) && validate!(string2)
    time = parse(string2)
    parse(string1) + (time.hour * 3600) + (time.min * 60) + time.sec 
  end

  def extract
    [day - Time.now.day , strftime("%T")]
  end
  
end
