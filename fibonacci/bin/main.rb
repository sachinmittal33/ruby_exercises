require_relative('../lib/integer.rb')

number = gets.chomp.to_i
number.generate_fibonacci do |prev_num,next_num| 
  puts next_num
  prev_num , next_num = next_num , prev_num + next_num
end
