require 'time'

class Time

  MATCH_PATTERN = /^([01]?[0-9]|2[0-3]):[0-5]?[0-9]:[0-5]?[0-9]$/

  def self.validate!(string)
    raise RangeError if string !~ MATCH_PATTERN
  end

  def self.sum_many(*values)
    result_time = parse("0:0:0")
    values.each do |value|
      validate!(value)
      time = parse(value)
      result_time += time.hour * 3600 + time.min * 60 + time.sec
    end
    result_time
  end

  def extract
    [day - Time.now.day , strftime("%T")]
  end

end
