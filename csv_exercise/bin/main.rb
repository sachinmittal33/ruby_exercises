require_relative '../lib/custom_array.rb'
require_relative '../lib/file.rb'
require 'csv'

input_path = File.absolute_path(__FILE__ + '/../../data/employee_data')
array = CSV.read(input_path, headers: true, header_converters: :symbol, col_sep: ", ")
hash = CustomArray.convert_to_hash(array)
output_path = File.absolute_path(__FILE__ + '/../../data/output.txt')
file = File.open(output_path, "w+")
file.write_from_hash(hash)
file.close
