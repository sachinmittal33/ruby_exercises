class File

  def write_from_hash(hash)
    hash.sort.each do |key, value|
      (value.size > 1) ? (puts "#{ key }s") : (puts key)
      value.each { |row| puts "#{ row[:name] } (empid: #{ row[:empid] })" }
      puts "\n"
    end
  end

end
