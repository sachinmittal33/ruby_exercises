class CustomArray < Array

  def self.convert_to_hash(array)
    array.sort_by { |row| [row[:empid], row] }.group_by { |row| row[:designation] }
  end

end
