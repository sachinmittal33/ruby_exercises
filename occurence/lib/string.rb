class String

  ALPHABET_CONST = /[a-zA-Z]/
  
  def count_of_alphabets_stored_in_hash
    occurence = Hash.new(0)
    each_char do |value| 
      if ALPHABET_CONST.match(value)
      occurence[value] += 1
      end
    end
    occurence 
  end

end

