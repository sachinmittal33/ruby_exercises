require_relative 'product.rb'

class Input

  def self.get
    begin
      product = Product.new
      puts "name of product:"
      product.name = gets.chomp
      puts "imported?"
      product.imported = gets.strip
      puts "exempted from sales tax?"
      product.sales_tax_exempted = gets.strip
      puts "price:"
      price = gets.strip
      Product.check_price(price)
      product.price = price.to_i
      puts "enter the quantity of item"
      quantity = gets.chomp.to_i
      [product , quantity]
    end
  end

end
