class PriceCalculator

  def self.sub_total(product, qty)
    product.total_price * qty 
  end

  def self.grand_total(list)
    total = 0
    list.each { |product, qty| total += sub_total(product, qty) }
    (total + 0.5).to_i
  end

end
