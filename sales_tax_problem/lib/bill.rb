require_relative 'price_calculator.rb'

class Bill

  def self.generate_and_display(list)
    print "Name".center(10) + "Price".center(10) + "SalesTax".center(10) + "ImportDuty".center(10)
    print "TotalPrice".center(15) + "Quantity".center(10) + "SubTotal".center(15) + "\n"
    list.each do |product, qty|
      print "#{product.name}".center(10)
      print "#{product.price.round(2)}".center(10)
      print "#{product.sales_tax.round(2)}".center(10)
      print "#{product.import_duty.round(2)}".center(10)
      print "#{product.total_price.round(2)}".center(15)
      print "#{qty}".center(10)
      print "#{PriceCalculator.sub_total(product, qty).round(2)}".center(15) + "\n"
    end
    puts "\nGrand Total :".ljust(65) + "#{PriceCalculator.grand_total(list)}".center(15)
  end

end
