class Product

  attr_accessor :name , :sales_tax_exempted , :imported , :price

  DIGIT_REGEX = /^[\d]*$/

  def self.check_price(price)
    raise RangeError if price !~ DIGIT_REGEX
  end

  def sales_tax
    (sales_tax_exempted == "no") ? (0.1 * price) : 0.0
  end

  def import_duty
    (imported == "yes") ? (0.05 * price) : 0.0
  end

  def total_price
    price + sales_tax + import_duty
  end

end
