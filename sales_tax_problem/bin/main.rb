require_relative '../lib/input.rb'
require_relative '../lib/bill.rb'
require_relative '../lib/cart.rb'

cart = Cart.new

begin
  product , quantity = Input.get
  cart.add_product(product, quantity)
  puts "do you want to add more(y/n)"
rescue RangeError => e
  puts e.message
  puts "do you want to add more(y/n)"
end while gets.chomp == "y"

Bill.generate_and_display(cart.list)
