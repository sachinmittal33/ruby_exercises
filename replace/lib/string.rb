
class String

  VOWEL_CONST = /[aeiou]/i
  
  def replace_using_regex
    gsub(VOWEL_CONST, '*')
  end

end

