require 'prime'

class Integer

  def find_prime_numbers
    prime_array = Array.new
    prime_array.push(2) if self >= 2
    (3..self).step(2) { |value| prime_array.push(value) if value.prime? }
    prime_array
  end

end
