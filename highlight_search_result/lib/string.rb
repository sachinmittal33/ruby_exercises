class String

  def highlight_search_and_count(pattern)
    count = 0
    match_pattern = Regexp.new(pattern, 'i')
    changed_string = gsub(match_pattern) do |pattern| 
      count += 1 
      "(#{pattern})"
    end
    [changed_string , count]
  end

end
