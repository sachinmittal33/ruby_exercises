class Array

  def element_size_hash
    group_by { |value| value.size }
  end
  
  def group_into_odd_even_size
    grouped_hash = Hash.new { |hash, key| hash[key] = [] }
    element_size_hash.sort.inject(0) do |_, (key, value)|
      if key.odd?
        grouped_hash['odd'].push(value)
      else
        grouped_hash['even'].push(value)
      end
    end
    grouped_hash
  end

end
