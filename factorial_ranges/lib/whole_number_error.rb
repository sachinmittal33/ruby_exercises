class WholeNumberError < StandardError

  def message
    "enter a positive integer"
  end

end
