require_relative '../lib/integer.rb'
require_relative '../lib/whole_number_error.rb'

begin
  puts "please enter an integer"
  number = gets.chomp
  raise WholeNumberError if number =~ /[\D*]/
  puts number.to_i.factorial
rescue WholeNumberError => e
  puts e.message
end
