class Array

  def element_size_hash
    group_by { |value| value.size }
  end
  
end
