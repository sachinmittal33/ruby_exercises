require_relative 'vehicle.rb'

class Bike < Vehicle

  attr_accessor :dealer

  def initialize(name, price, dealer)
    super(name, price)
    self.dealer = dealer
  end
  
  def to_s
    "#{ super }categorised as bike owned by #{ dealer }"
  end

end

