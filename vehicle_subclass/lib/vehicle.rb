class Vehicle

  attr_accessor :price
  attr_accessor :name

  def initialize(name, price)
    self.name = name
    self.price = price
  end
  
  def to_s
    "#{ name } is priced at #{ price }rs "
  end

end

