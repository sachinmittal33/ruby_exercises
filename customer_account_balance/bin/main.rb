require_relative '../lib/customer.rb'

customer_no1 = Customer.new('akaash')
puts "select options\n1 for deposit\n2 for withdraw\n3 for showing customer details"
selected_option = gets.chomp.to_i

case selected_option
when 1
  puts "enter the amount to be deposited"
  amount = gets.chomp
  customer_no1.deposit(amount)
  puts customer_no1
when 2
  puts "enter the amount to be withdrawn"
  amount = gets.chomp
  customer_no1.withdraw(amount)
  puts customer_no1
when 3
  puts customer_no1
else
  puts "wrong selection entered"
end
