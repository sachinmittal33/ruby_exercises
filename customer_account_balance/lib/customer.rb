class Customer

  @@customer_count = 0

  NON_DIGIT_REGEX = /[\D*]/

  def initialize(name)
    @name = name
    @balance = 1000
    @@customer_count += 1
    @account_no = @@customer_count
  end

  def validate!(amount)
    raise RangeError, "invalid input" if amount =~ NON_DIGIT_REGEX
  end

  def deposit(amount)
    validate!(amount)
    @balance += amount.to_i
  end

  def withdraw(amount)
    validate!(amount)
    if amount.to_i < @balance
      @balance -= amount.to_i
    else
      puts "not sufficient balance"
    end
  end

  def to_s
    "#{ @name } is a customer with account no #{ @account_no } having balance as #{ @balance } rs"
  end

end
