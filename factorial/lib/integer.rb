require_relative 'custom_error.rb'

class Integer

  def factorial!
    raise CustomError if self < 0
    (1..self).inject(:*)
  end

end
