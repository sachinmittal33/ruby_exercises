require_relative '../lib/integer.rb'

puts "enter a non negative integer whose factorial to be found"
begin 
  puts Integer(gets.chomp).factorial!
rescue CustomError => obj
  puts obj.message
end
