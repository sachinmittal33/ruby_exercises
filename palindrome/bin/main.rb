require_relative'../lib/string.rb'

string = gets.chomp
if string =~ /[q]/i
  exit
else
  if string.palindrome?
    puts "string is palindrome"
  else
    puts "string is not palindrome"
  end
end

