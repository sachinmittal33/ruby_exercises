class Interest

  attr_accessor :principal , :time

  RATE = 10.0

  def rate
    RATE
  end

#here a method named as rate is defined for getting the value of interest rate
#this method defined for rate also benefits for changing the rate according to
#some required situation . Also anyone can view this rate but it cannot be
#changed by any one outside the class.

  def initialize
    yield self if block_given?
    raise StandardError if (principal.nil?) || (time.nil?)
  end

#A standard error is raised if anyone of the principal or time is not entered
#if anyone of them is nil.

  def simple
    principal * rate / 100 * time
  end

  def compound
    principal * ((1 + rate / 100) ** time) - principal
  end

  def difference_in_simple_and_compound
    if time > 1.0
      compound - simple
    else
      0.0
    end
  end

end
