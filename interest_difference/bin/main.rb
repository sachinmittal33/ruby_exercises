require_relative '../lib/interest.rb'

puts "enter the principal amount"
principal = gets.chomp.to_f
puts "enter the time in years"
time = gets.chomp.to_f

begin
  interest = Interest.new do |object|
    object.principal = principal
  end
  puts "simple interest is #{interest.simple}"
  puts "compound interest is #{interest.compound}"
  puts "difference between simple and compound interest is #{interest.difference_in_simple_and_compound}"
rescue StandardError => e
  puts e.message
end

#Raised error is rescued and and a message is shown
#also process ahead is not done if error is raised
