class String

  UPPERCASE_RANGE = ('A'..'Z')
  LOWERCASE_RANGE = ('a'..'z')
  DIGIT_RANGE = ('0'..'9')

  def character_count
    lowercase_count , uppercase_count , digit_count , specialchar_count = 0 , 0 , 0 , 0
    each_char do |value|
      case value
      when UPPERCASE_RANGE
        lowercase_count += 1
      when LOWERCASE_RANGE
        uppercase_count += 1
      when DIGIT_RANGE
        digit_count += 1
      else
        specialchar_count += 1
      end
    end
    [lowercase_count , uppercase_count , digit_count , specialchar_count]
  end

end